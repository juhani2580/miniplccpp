#pragma once

#include "Alt.hpp"

enum struct Primitive : i64
{
    invalid,  // "unset" type for internal use
    devoid,   // "typeless" type for internal use
    integer,
    real,
    text,
};

struct Type
{
    Primitive primitive  = Primitive::invalid;
    i64       array_size = -1;
    b8        is_var     = false;

    auto is_invalid() const -> b8
    {
        return primitive == Primitive::invalid;
    }

    auto is_devoid() const -> b8
    {
        return primitive == Primitive::devoid && array_size == -1;
    }

    auto is_integer() const -> b8
    {
        return primitive == Primitive::integer && array_size == -1;
    }

    auto is_real() const -> b8
    {
        return primitive == Primitive::real && array_size == -1;
    }

    auto is_text() const -> b8
    {
        return primitive == Primitive::text && array_size == -1;
    }

    auto operator==(const Type& rhs) const -> b8
    {
        return primitive == rhs.primitive && array_size == rhs.array_size;
    }

    auto operator!=(const Type& rhs) const -> b8
    {
        return !(*this == rhs);
    }

    operator const char*() const
    {
        switch (primitive)
        {
        default:
            return "_devoid";
        case Primitive::integer:
            return "_integer";
        case Primitive::real:
            return "_real";
        case Primitive::text:
            return "_string";
        }
    }
};
