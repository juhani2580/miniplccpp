#pragma once

#include "Alt.hpp"
#include "Type.hpp"

struct SymbolTable
{
    std::map<std::tuple<i64, std::string>, Type> symtab;

    auto find(const std::vector<i64>& scopes, const std::string& id) -> Type;
};
