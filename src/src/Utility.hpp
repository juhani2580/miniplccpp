#pragma once

#include "Alt.hpp"

void string_replace(std::string&       text,
                    const std::string& search,
                    const std::string& replace);

struct Error : std::exception
{
    Error() = default;

    Error(std::string message) : m_message(std::move(message))
    {
    }

    [[nodiscard]] auto what() const noexcept -> const char* override
    {
        return m_message.data();
    }

private:
    std::string m_message{};
};
