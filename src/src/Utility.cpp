#include "Utility.hpp"

void string_replace(std::string&       text,
                    const std::string& search,
                    const std::string& replace)
{
    u64 pos = 0;
    while ((pos = text.find(search, pos)) != std::string::npos)
    {
        text.replace(pos, search.length(), replace);
        pos += replace.length();
    }
}
