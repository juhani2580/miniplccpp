#pragma once

#include "Alt.hpp"
#include "Ast.hpp"
#include "SymbolTable.hpp"
#include "Utility.hpp"

struct CodeGenerator : AstVisitor
{
    CodeGenerator(SymbolTable& symbol_table);
    void visit(SimpleTypeAst& /*ast*/) override;
    void visit(ArrayTypeAst& /*ast*/) override;
    void visit(IntegerExprAst& ast) override;
    void visit(RealExprAst& ast) override;
    void visit(TextExprAst& ast) override;
    void visit(IdExprAst& ast) override;
    void visit(NotExprAst& ast) override;
    void visit(BinaryExprAst& ast) override;
    void visit(CallExprAst& ast) override;
    void visit(ReturnAst& ast) override;
    void visit(IfAst& ast) override;
    void visit(WhileAst& ast) override;
    void visit(CallAst& ast) override;
    void visit(AssignAst& ast) override;
    void visit(VarDeclAst& ast) override;
    void visit(BlockAst& ast) override;
    void visit(ScopedBlockAst& ast) override;
    void visit(ParameterAst& ast) override;
    void visit(FunctionAst& ast) override;
    void visit(ProgramAst& ast) override;

private:
    std::vector<std::string> m_codes{};

    static auto gen_id() -> std::string;

    template <typename S, typename... Args>
    void gen(const S& format_str, Args&&... args);

    template <typename S, typename... Args>
    void push(const S& format_str, Args&&... args);

    auto pop() -> std::string;
};
