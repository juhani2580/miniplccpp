#include "Alt.hpp"
#include "CodeGenerator.hpp"
#include "Parser.hpp"
#include "Scanner.hpp"
#include "SemanticAnalyzer.hpp"
#include "SymbolTable.hpp"
#include "SymbolTablePopulator.hpp"

auto main(int argc, char** argv) -> int
try
{
    if (argc < 2)
    {
        fmt::print("more params\n");
        std::exit(0);
    }

    Scanner scanner(std::make_unique<std::ifstream>(argv[1]));

    fmt::print("SCANNED TOKENS:\n");
    for (Tag tag = scanner.scan(); tag != Tag::fatal_error;
         tag     = scanner.scan())
    {
        fmt::print("{}\n", scanner);
        if (tag == Tag::eof)
        {
            break;
        }
    }
    fmt::print("\n");
    scanner.reset();

    Parser                      parser(scanner);
    std::unique_ptr<ProgramAst> program_ast;
    fmt::print("PARSED TOKENS:\n");
    program_ast = parser.parse();
    fmt::print("\n");

    fmt::print("SYMBOL TABLE:\n");
    SymbolTable          symbol_table{};
    SymbolTablePopulator symbol_table_populator(symbol_table);
    program_ast->accept(symbol_table_populator);
    fmt::print("\n");

    fmt::print("SEMANTIC ANALYZER:\n");
    SemanticAnalyzer semantic_analyzer(symbol_table);
    program_ast->accept(semantic_analyzer);
    fmt::print("\n");

    fmt::print("CODEGEN:\n");
    CodeGenerator code_generator(symbol_table);
    program_ast->accept(code_generator);
}
catch (const Error& e)
{
    fmt::print("{}\n", e.what());
}
