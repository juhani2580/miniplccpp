#include "CodeGenerator.hpp"

namespace
{
auto codegen_error = Error("Code generation failed.");
}

CodeGenerator::CodeGenerator(SymbolTable& symbol_table) :
    AstVisitor(symbol_table)
{
}

void CodeGenerator::visit(SimpleTypeAst& /*ast*/)
{
}

void CodeGenerator::visit(ArrayTypeAst& /*ast*/)
{
}

void CodeGenerator::visit(IntegerExprAst& ast)
{
    std::string id = gen_id();
    push(id);
    push("_integer {} = {};\n", id, ast.value);
}

void CodeGenerator::visit(RealExprAst& ast)
{
    std::string id = gen_id();
    push(id);
    push("_real {} = {};\n", id, ast.value);
}

void CodeGenerator::visit(TextExprAst& ast)
{
    std::string id = gen_id();

    push(id);
    push(  // FIXME
        "_string {} = malloc({});\n"
        "{} = \"{}\";\n",
        id,
        ast.value.size() + 1,
        id,
        ast.value);
}

void CodeGenerator::visit(IdExprAst& ast)
{

    std::string id = gen_id();
    push(id);
    push("_integer {} = {};\n", id, ast.id);
}

void CodeGenerator::visit(NotExprAst& ast)
{
    ast.rhs->accept(*this);
    std::string rhs    = pop();
    std::string rhs_id = pop();

    std::string id = gen_id();
    push(id);
    push(
        "{}"
        "_integer {} = !{};\n",
        rhs,
        id,
        rhs_id);
}

void CodeGenerator::visit(BinaryExprAst& ast)
{
    ast.lhs->accept(*this);
    std::string lhs    = pop();
    std::string lhs_id = pop();

    ast.rhs->accept(*this);
    std::string rhs    = pop();
    std::string rhs_id = pop();

    std::string id = gen_id();
    std::string op;
    switch (ast.op)
    {
    default:
        op = "";
        break;
    case Tag::k_minus:
        op = "-";
        break;
    case Tag::k_plus:
        op = "+";
        break;
    case Tag::k_mul:
        op = "*";
        break;
    case Tag::k_div:
        op = "/";
        break;
    case Tag::k_mod:
        op = "%";
        break;
    case Tag::k_lt:
        op = "<";
        break;
    case Tag::k_lte:
        op = "<=";
        break;
    case Tag::k_gt:
        op = ">";
        break;
    case Tag::k_gte:
        op = ">=";
        break;
    case Tag::k_eq:
        op = "==";
        break;
    case Tag::k_neq:
        op = "!=";
        break;
    }

    push(id);
    push(
        "{}"
        "{}"
        "integer {} = {} {} {};\n",
        lhs,
        rhs,
        id,
        lhs_id,
        op,
        rhs_id);
}

void CodeGenerator::visit(CallExprAst& ast)
{
    std::string id = gen_id();
    std::string expr_codes{};
    std::string call_expr = fmt::format("{}(", ast.id);

    if (!ast.args.empty())
    {
        ast.args[0]->accept(*this);
        expr_codes += pop();
        call_expr += pop();
        for (u64 i = 1; i < ast.args.size(); i++)
        {
            call_expr += ", ";
            ast.args[i]->accept(*this);
            expr_codes += pop();
            call_expr += pop();
        }
    }
    call_expr += ")";
    push(id);
    push(
        "{}"
        "{};\n",
        expr_codes,
        call_expr);
}

void CodeGenerator::visit(ReturnAst& ast)
{
    std::string return_code{};
    if (ast.expr)
    {
        ast.expr->accept(*this);
        std::string expr    = pop();
        std::string expr_id = pop();
        return_code += fmt::format(
            "{}"
            "return {};\n",
            expr,
            expr_id);
    }
    else
    {
        return_code += fmt::format("return;\n");
    }

    push("{}", return_code);
}

void CodeGenerator::visit(IfAst& ast)
{
    ast.condition->accept(*this);
    std::string condition    = pop();
    std::string condition_id = pop();

    std::string body_label = gen_id();
    std::string else_label = gen_id();
    std::string end_label  = gen_id();

    ast.then_stmt->accept(*this);
    std::string then_code = pop();

    std::string else_code = "";
    if (ast.else_stmt)
    {
        ast.else_stmt->accept(*this);
        else_code = pop();
    }

    push(
        "{}"
        "if ({}) goto {};\n"
        "goto {};\n"

        "{}:\n"
        "{}"
        "goto {};\n"

        "{}:\n"
        "{}"
        "goto {};\n"
        "{}:\n",

        condition,
        condition_id,
        body_label,
        else_label,

        body_label,
        then_code,
        end_label,

        else_label,
        else_code,
        end_label,
        end_label);
}

void CodeGenerator::visit(WhileAst& ast)
{
    std::string condition_label = gen_id();
    std::string body_label      = gen_id();

    std::string while_code = fmt::format(
        "goto {};\n"
        "{}:\n",
        condition_label,
        body_label);

    ast.stmt->accept(*this);
    std::string body_code = pop();

    ast.condition->accept(*this);
    std::string condition    = pop();
    std::string condition_id = pop();

    push(
        "goto {};\n"
        "{}:\n"
        "{}"
        "{}:\n"
        "{}"
        "if ({}) goto {};\n",
        condition_label,
        body_label,
        body_code,
        condition_label,
        condition,
        condition_id,
        body_label);
}

void CodeGenerator::visit(CallAst& ast)
{
    ast.call_expr->accept(*this);
    std::string expr = pop();
    pop();
    push("{}", expr);
}

void CodeGenerator::visit(AssignAst& ast)
{
    ast.value->accept(*this);
    std::string expr    = pop();
    std::string expr_id = pop();
    push(
        "{}"
        "{} = {};\n",
        expr,
        ast.id,
        expr_id);
}

void CodeGenerator::visit(VarDeclAst& ast)
{
    Type type       = ast.type->type();
    i64  array_size = type.array_size;

    std::string ids{};
    if (!ast.ids.empty())
    {
        ids += ast.ids[0];
        for (u64 i = 1; i < ast.ids.size(); i++)
        {
            ids += fmt::format(", {}", ast.ids[i]);
        }
    }

    if (array_size == -1)
    {
        push("{} {};\n", type, ids);
    }
    else
    {
        push("{} {}[{}];\n", type, ids, array_size);
    }
}

void CodeGenerator::visit(BlockAst& ast)
{
    std::string stmts{};
    for (auto&& stmt : ast.stmts)
    {
        stmt->accept(*this);
        stmts += fmt::format("{}", pop());
    }

    push(
        "{{\n"
        "{}"
        "}}\n",
        stmts);
}

void CodeGenerator::visit(ScopedBlockAst& ast)
{
    ast.block->accept(*this);
}

void CodeGenerator::visit(ParameterAst& ast)
{
    Type type = find(ast.id);
    push("{}{} {}", type, type.is_var ? "*" : "", ast.id);
}

void CodeGenerator::visit(FunctionAst& ast)
{
    std::string func{};
    Type return_type = ast.return_type ? ast.return_type->type() : Type{};

    func += fmt::format("{} {}(", return_type, ast.id);
    if (!ast.params.empty())
    {
        ast.params[0]->accept(*this);
        func += pop();
        for (u64 i = 1; i < ast.params.size(); i++)
        {
            func += ", ";
            ast.params[i]->accept(*this);
            func += pop();
        }
    }
    func += ")\n";

    ast.block->accept(*this);
    func += pop();
    push("{}", func);
}

void CodeGenerator::visit(ProgramAst& ast)
{
    gen(R"RAW(
////////////////////////////////////////////////////////////////////////////////

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

typedef void      _devoid;
typedef long long _boolean;
typedef long long _integer;
typedef double    _real;
typedef char*     _string;

#define true  1
#define false 0

void _read(_integer* ptr)
{{
    scanf("%d", ptr);
}}

void _writeln(_integer ptr)
{{
    printf("%s\n", ptr);
}}

void _assert(_integer cond)
{{
    assert(cond);
}}

////////////////////////////////////////////////////////////////////////////////

)RAW");

    for (auto&& func : ast.funcs)
    {
        func->accept(*this);
        gen("{}\n", pop());
    }

    ast.block->accept(*this);
    gen("int main(int argc, char** argv)\n"
        "{}\n",
        pop());
}

// ----------------------------------------------------------------------------

auto CodeGenerator::gen_id() -> std::string
{
    static i64 counter = -1;
    counter++;
    return fmt::format("_r{}", counter);
}

template <typename S, typename... Args>
void CodeGenerator::gen(const S& format_str, Args&&... args)
{
    fmt::print(format_str, args...);
}

template <typename S, typename... Args>
void CodeGenerator::push(const S& format_str, Args&&... args)
{
    m_codes.push_back(fmt::format(format_str, args...));
}

auto CodeGenerator::pop() -> std::string
{
    std::string ret = m_codes.back();
    m_codes.pop_back();
    return ret;
}
