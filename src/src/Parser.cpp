#include "Parser.hpp"

namespace
{

auto gen_scope() -> i64
{
    static i64 counter = 0;
    return counter++;
}

}  // namespace

#define format_parse_error()                                                   \
    ({                                                                         \
        fmt::format(                                                           \
            "\nParse error:\n"                                                 \
            "{}:{}:{}\n"                                                       \
            "token: {}\n",                                                     \
            __FILE__,                                                          \
            __func__,                                                          \
            __LINE__,                                                          \
            m_scanner);                                                        \
    })

#define parse_error() ({ ParseError(format_parse_error()); })

#define expect(token)                                                          \
    {                                                                          \
        Tag _token = (token);                                                  \
        if (tag() != _token)                                                   \
        {                                                                      \
            throw ParseError(                                                  \
                fmt::format("{}"                                               \
                            "expected: {}\n",                                  \
                            format_parse_error(),                              \
                            _token));                                          \
        }                                                                      \
    }

Parser::Parser(Scanner& scanner) : m_scanner(scanner)
{
}

auto Parser::parse() -> std::unique_ptr<ProgramAst>
{
    m_scanner.scan();  // init scanner
    auto ret = parse_program();
    expect(Tag::eof);
    return ret;
}

// Private methods
// ============================================================================

[[nodiscard]] auto Parser::tag() const -> Tag
{
    return m_scanner.tag();
}

[[nodiscard]] auto Parser::line() const -> i64
{
    return m_scanner.line();
}

[[nodiscard]] auto Parser::col() const -> i64
{
    return m_scanner.col();
}

[[nodiscard]] auto Parser::text() const -> std::string
{
    return m_scanner.text();
}

[[nodiscard]] auto Parser::integer() const -> i64
{
    return m_scanner.integer();
}

[[nodiscard]] auto Parser::real() const -> f64
{
    return m_scanner.real();
}

[[nodiscard]] auto Parser::precedence() const -> i64
{
    switch (tag())
    {
    default:
        return -1;
    case Tag::k_eq:
    case Tag::k_neq:
    case Tag::k_lt:
    case Tag::k_lte:
    case Tag::k_gt:
    case Tag::k_gte:
        return 200;
    case Tag::k_minus:
    case Tag::k_plus:
    case Tag::k_or:
        return 300;
    case Tag::k_mul:
    case Tag::k_div:
    case Tag::k_mod:
    case Tag::k_and:
        return 400;
    case Tag::k_dot:
    case Tag::k_not:
        return 500;
    }
}

auto Parser::unsafe_scan() -> Tag
{
    fmt::print("{}\n", m_scanner);
    return m_scanner.scan();
}

auto Parser::scan() -> Tag
{
    Tag ret = unsafe_scan();
    if (ret == Tag::eof || ret == Tag::fatal_error)
    {  // eof is handled inside parse()
        throw parse_error();
    }
    return ret;
}

#define consume(token)                                                         \
    {                                                                          \
        expect((token));                                                       \
        scan();                                                                \
    }

#define scan_id()                                                              \
    ({                                                                         \
        std::string _ret = text();                                             \
        scan();                                                                \
        _ret;                                                                  \
    })

#define expect_id()                                                            \
    ({                                                                         \
        expect(Tag::id);                                                       \
        scan_id();                                                             \
    })

auto Parser::attempt(Tag token) -> b8
{
    if (token == tag())
    {
        scan();
        return true;
    }
    return false;
}

auto Parser::parse_type() -> std::unique_ptr<TypeAst>
{
    if (tag() == Tag::id)
    {
        return parse_simple_type();
    }
    expect(Tag::k_array);
    return parse_array_type();
}

auto Parser::parse_simple_type() -> std::unique_ptr<SimpleTypeAst>
{
    auto ret = std::make_unique<SimpleTypeAst>();
    ret->id  = scan_id();
    return ret;
}

auto Parser::parse_array_type() -> std::unique_ptr<ArrayTypeAst>
{
    auto ret = std::make_unique<ArrayTypeAst>();
    scan();  // array
    consume(Tag::k_l_sqbr);
    if (tag() != Tag::k_r_sqbr)
    {
        ret->size = parse_integer_expr();
    }
    consume(Tag::k_r_sqbr);
    consume(Tag::k_of);
    ret->element_type = parse_simple_type();
    return ret;
}

auto Parser::parse_expr() -> std::unique_ptr<ExprAst>
{
    auto lhs = parse_primary_expr();
    return parse_bin_op_rhs_expr(0, std::move(lhs));
}

auto Parser::parse_primary_expr() -> std::unique_ptr<ExprAst>
{
    switch (tag())
    {
    default:
        throw parse_error();
    case Tag::integer:
        return parse_integer_expr();
    case Tag::real:
        return parse_real_expr();
    case Tag::text:
        return parse_text_expr();
    case Tag::id:
        return parse_call_or_id_expr();
    case Tag::k_l_paren:
        return parse_paren_expr();
    case Tag::k_not:
        return parse_unary_expr();
    }
}

auto Parser::parse_bin_op_rhs_expr(i64                      expr_precedence,
                                   std::unique_ptr<ExprAst> lhs)
    -> std::unique_ptr<ExprAst>
{
    Tag prev_tag = Tag::invalid_token;
    while (true)
    {
        i64 tok_precedence = precedence();

        // Hack to disallow chained relation operators
        // eg. a < b < c
        if ((prev_tag == Tag::k_eq || prev_tag == Tag::k_neq ||
             prev_tag == Tag::k_lt || prev_tag == Tag::k_lt ||
             prev_tag == Tag::k_gt || prev_tag == Tag::k_lte ||
             prev_tag == Tag::k_gte) &&
            (tag() == Tag::k_eq || tag() == Tag::k_neq || tag() == Tag::k_lt ||
             tag() == Tag::k_lt || tag() == Tag::k_gt || tag() == Tag::k_lte ||
             tag() == Tag::k_gte))
        {
            throw parse_error();
        }

        if (tok_precedence < expr_precedence)
        {
            return lhs;
        }

        prev_tag = tag();
        scan();
        auto rhs = parse_primary_expr();
        if (tok_precedence < precedence())
        {
            rhs = parse_bin_op_rhs_expr(tok_precedence + 1, std::move(rhs));
        }

        auto bin = std::make_unique<BinaryExprAst>();
        bin->op  = prev_tag;
        bin->lhs = std::move(lhs);
        bin->rhs = std::move(rhs);
        lhs      = std::move(bin);
    }
}

auto Parser::parse_integer_expr() -> std::unique_ptr<IntegerExprAst>
{
    auto ret   = std::make_unique<IntegerExprAst>();
    ret->value = integer();
    scan();  // integer
    return ret;
}

auto Parser::parse_real_expr() -> std::unique_ptr<RealExprAst>
{
    auto ret   = std::make_unique<RealExprAst>();
    ret->value = real();
    scan();  // real
    return ret;
}

auto Parser::parse_text_expr() -> std::unique_ptr<TextExprAst>
{
    auto ret   = std::make_unique<TextExprAst>();
    ret->value = text();
    scan();  // text
    return ret;
}

auto Parser::parse_call_or_id_expr() -> std::unique_ptr<ExprAst>
{
    std::string id = scan_id();

    // call
    if (tag() == Tag::k_l_paren)
    {
        return parse_call_expr_tail(std::move(id));
    }

    // IdExpr
    auto ret = std::make_unique<IdExprAst>();
    ret->id  = id;
    if (attempt(Tag::k_l_sqbr))
    {
        ret->index = parse_expr();
        consume(Tag::k_r_sqbr);
    }
    return ret;
}

auto Parser::parse_call_expr_tail(std::string id)
    -> std::unique_ptr<CallExprAst>
{
    auto ret = std::make_unique<CallExprAst>();
    ret->id  = std::move(id);
    scan();  // k_l_paren
    if (!attempt(Tag::k_r_paren))
    {
        while (true)
        {
            ret->args.push_back(parse_expr());
            if (attempt(Tag::k_r_paren))
            {
                break;
            }
            consume(Tag::k_comma);
        }
    }
    return ret;
}

auto Parser::parse_paren_expr() -> std::unique_ptr<ExprAst>
{
    scan();  // k_l_paren
    auto ret = parse_expr();
    consume(Tag::k_r_paren);
    return ret;
}

auto Parser::parse_unary_expr() -> std::unique_ptr<NotExprAst>
{
    auto ret = std::make_unique<NotExprAst>();
    scan();  // k_not
    ret->rhs = parse_expr();
    return ret;
}

auto Parser::parse_stmt() -> std::unique_ptr<StmtAst>
{
    switch (tag())
    {
    case Tag::k_var:
        return parse_var_decl();
    case Tag::k_return:
        return parse_return();
    case Tag::k_begin:
        return parse_scoped_block();
    case Tag::k_if:
        return parse_if();
    case Tag::k_while:
        return parse_while();
    case Tag::id:
        return parse_call_or_assign();
    default:
        assert(0);
    }
}

auto Parser::parse_call_or_assign() -> std::unique_ptr<StmtAst>
{
    std::string id = scan_id();

    // call
    if (tag() == Tag::k_l_paren)
    {
        auto call       = std::make_unique<CallAst>();
        call->call_expr = parse_call_expr_tail(id);
        return call;
    }

    // assign
    auto ret = std::make_unique<AssignAst>();
    ret->id  = id;
    consume(Tag::k_assign);
    ret->value = parse_expr();
    return ret;
}

auto Parser::parse_var_decl() -> std::unique_ptr<VarDeclAst>
{
    auto ret = std::make_unique<VarDeclAst>();
    scan();  // var
    do
    {
        ret->ids.push_back(scan_id());
    } while (attempt(Tag::k_comma));
    consume(Tag::k_colon);
    ret->type = parse_type();
    return ret;
}

auto Parser::parse_return() -> std::unique_ptr<ReturnAst>
{
    auto ret = std::make_unique<ReturnAst>();
    scan();  // return
    if (tag() != Tag::k_semicolon && tag() != Tag::k_end)
    {
        ret->expr = parse_expr();
    }
    return ret;
}

auto Parser::parse_while() -> std::unique_ptr<WhileAst>
{
    auto ret = std::make_unique<WhileAst>();
    scan();  // while
    ret->condition = parse_expr();
    consume(Tag::k_do);
    ret->stmt = parse_stmt();
    return ret;
}

auto Parser::parse_if() -> std::unique_ptr<IfAst>
{
    auto ret = std::make_unique<IfAst>();
    scan();  // if
    ret->condition = parse_expr();
    consume(Tag::k_then);
    ret->then_stmt = parse_stmt();
    if (attempt(Tag::k_semicolon) && attempt(Tag::k_else))
    {
        ret->else_stmt = parse_stmt();
    }
    return ret;
}

auto Parser::parse_scoped_block() -> std::unique_ptr<ScopedBlockAst>
{
    auto ret   = std::make_unique<ScopedBlockAst>();
    ret->scope = gen_scope();
    m_scopes.push(ret->scope);

    ret->block = parse_block();

    m_scopes.pop();
    return ret;
}

auto Parser::parse_block() -> std::unique_ptr<BlockAst>
{
    auto ret = std::make_unique<BlockAst>();
    scan();  // begin
    ret->stmts.push_back(parse_stmt());
    attempt(Tag::k_semicolon);
    while (tag() != Tag::k_end)
    {
        ret->stmts.push_back(parse_stmt());
        attempt(Tag::k_semicolon);
    }
    consume(Tag::k_end);
    return ret;
}

auto Parser::parse_function() -> std::unique_ptr<FunctionAst>
{
    auto ret   = std::make_unique<FunctionAst>();
    ret->scope = gen_scope();
    m_scopes.push(ret->scope);

    b8 is_func = tag() == Tag::k_function;
    scan();  // procedure | function
    ret->id = expect_id();

    consume(Tag::k_l_paren);
    if (tag() != Tag::k_r_paren)
    {
        do
        {
            auto param    = std::make_unique<ParameterAst>();
            param->is_var = attempt(Tag::k_var);
            param->id     = expect_id();
            consume(Tag::k_colon);
            param->type = parse_type();
            ret->params.push_back(std::move(param));
        } while (attempt(Tag::k_comma));
    }
    consume(Tag::k_r_paren);

    if (is_func)
    {
        consume(Tag::k_colon);
        ret->return_type = parse_type();
    }
    consume(Tag::k_semicolon);

    expect(Tag::k_begin);
    ret->block = parse_block();
    consume(Tag::k_semicolon);

    m_scopes.pop();
    return ret;
}

auto Parser::parse_program() -> std::unique_ptr<ProgramAst>
{
    auto ret   = std::make_unique<ProgramAst>();
    ret->scope = gen_scope();
    m_scopes.push(ret->scope);

    consume(Tag::k_program);
    ret->id = expect_id();
    consume(Tag::k_semicolon);

    while (tag() == Tag::k_procedure || tag() == Tag::k_function)
    {
        try
        {
            ret->funcs.push_back(parse_function());
        }
        catch (const ParseError& e)
        {
            if (tag() == Tag::fatal_error)
            {
                throw;
            }
            fmt::print(
                "{}\n"
                "SKIPPING:\n",
                e.what());
            while (!attempt(Tag::k_end))
            {
                scan();
            }
            consume(Tag::k_semicolon);
            fmt::print("\nSKIPPED TO: {}\n", m_scanner);
        }
    }

    expect(Tag::k_begin);
    ret->block = parse_scoped_block();
    expect(Tag::k_dot);
    unsafe_scan();  // dot

    m_scopes.pop();
    return ret;
}
