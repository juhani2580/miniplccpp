// "Alternate Standard Library"
#pragma once

#include <cassert>
#include <climits>
#include <cstddef>
#include <cstdlib>
#include <fstream>
#include <istream>
#include <map>
#include <stack>
#include <utility>
#include <vector>

#include "fmt/format.h"
#include "fmt/printf.h"

using namespace fmt::literals;

// Type
// ============================================================================

using b8  = std::conditional<sizeof(bool) == 1, bool, uint8_t>::type;
using u8  = std::uint8_t;
using u16 = std::uint16_t;
using u32 = std::uint32_t;
using u64 = std::uint64_t;
using i8  = std::int8_t;
using i16 = std::int16_t;
using i32 = std::int32_t;
using i64 = std::int64_t;
using f32 = float;
using f64 = double;

// won't work with C++14 :(
// inline constexpr u8  U8_MAX  = UINT8_MAX;
// inline constexpr u16 U16_MAX = UINT16_MAX;
// inline constexpr u32 U32_MAX = UINT32_MAX;
// inline constexpr u64 U64_MAX = UINT64_MAX;
// inline constexpr i8  I8_MIN  = INT8_MIN;
// inline constexpr i16 I16_MIN = INT16_MIN;
// inline constexpr i32 I32_MIN = INT32_MIN;
// inline constexpr i64 I64_MIN = INT64_MIN;
// inline constexpr i8  I8_MAX  = INT8_MAX;
// inline constexpr i16 I16_MAX = INT16_MAX;
// inline constexpr i32 I32_MAX = INT32_MAX;
// inline constexpr i64 I64_MAX = INT64_MAX;
// inline constexpr f32 F32_MIN = FLT_MIN;
// inline constexpr f64 F64_MIN = DBL_MIN;
// inline constexpr f32 F32_MAX = FLT_MAX;
// inline constexpr f64 F64_MAX = DBL_MAX;

constexpr auto operator"" _i8(unsigned long long x) -> i8
{
    return static_cast<i8>(x);
}

constexpr auto operator"" _i16(unsigned long long x) -> i16
{
    return static_cast<i16>(x);
}

constexpr auto operator"" _i32(unsigned long long x) -> i32
{
    return static_cast<i32>(x);
}

constexpr auto operator"" _i64(unsigned long long x) -> i64
{
    return static_cast<i64>(x);
}

constexpr auto operator"" _u8(unsigned long long x) -> u8
{
    return static_cast<u8>(x);
}

constexpr auto operator"" _u16(unsigned long long x) -> u16
{
    return static_cast<u16>(x);
}

constexpr auto operator"" _u32(unsigned long long x) -> u32
{
    return static_cast<u32>(x);
}

constexpr auto operator"" _u64(unsigned long long x) -> u64
{
    return static_cast<u64>(x);
}

// Miscallenous
// ============================================================================

#define die(message)                                                           \
    ({                                                                         \
        fmt::print("{}, {}, {}\n", __FILE__, __func__, __LINE__);              \
        _alt::_die(message);                                                   \
    })

namespace _alt
{

[[noreturn]] inline void _die(const char* message)
{
    fmt::print("FATAL: {}\n", message);
    if (errno > 0)
    {
        fmt::print("errno: {}\n", strerror(errno));
    }
    std::abort();
}

}  // namespace _alt

#define isizeof(x) static_cast<i64>(sizeof(x))

static_assert(CHAR_BIT == 8);
static_assert(isizeof(void*) >= 8);
static_assert(isizeof(int) >= 4);
static_assert(isizeof(f32) >= 4);
static_assert(isizeof(f64) >= 8);

// Better min and max
// ----------------------------------------------------------------------------

template <typename T>
inline auto min(T x, T y) -> T
{
    return (y < x) ? y : x;
}

template <typename T, typename... TArgs>
inline auto min(T x, T y, TArgs... args) -> T
{
    return min(min(x, y), args...);
}

template <typename T>
inline auto max(T x, T y) -> T
{
    return (x < y) ? y : x;
}

template <typename T, typename... TArgs>
inline auto max(T x, T y, TArgs... args) -> T
{
    return max(max(x, y), args...);
}

// Memory
// ============================================================================

namespace mem
{

inline void copy(const void* in, i64 size, void* out)
{
    std::memcpy(out, in, static_cast<u64>(size));
}

inline void move(void* in, i64 size, void* out)
{
    std::memmove(out, in, static_cast<u64>(size));
}

inline void set(u8 byte, i64 size, void* out)
{
    std::memset(out, byte, static_cast<u64>(size));
}

// Prevent variable from being optimized away by the compiler.
// See: https://youtu.be/nXaxk27zwlk?t=2441
inline void escape(void* p)
{
    asm volatile("" : : "g"(p) : "memory");
}

// Memory barrier, which prevents reordering instructions during
// compile time.
inline void barrier()
{
    asm volatile("" : : : "memory");
}

}  // namespace mem

// Math
// ============================================================================

// Return next power of two for the given argument.
//
// Return zero for zero argument.
inline auto next_pow2(i64 x) -> i64
{
    if (x == 1)
    {
        return 1;
    }
    return 1 << (64 - __builtin_clzll(static_cast<u64>(x) - 1));
}
