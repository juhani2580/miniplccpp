#include "Scanner.hpp"

#include "Utility.hpp"

Scanner::Scanner(std::unique_ptr<std::istream> in) :
    m_in(std::move(in)),
    m_start(m_in->tellg())
{
    m_in->exceptions(std::istream::eofbit | std::istream::failbit |
                     std::istream::badbit);
}

auto Scanner::scan() -> Tag
{
    try
    {
    again:
        m_tag     = Tag::fatal_error;  // just in case
        m_text    = "";
        m_integer = 0;
        m_real    = 0.0;

        while (whitespace())
        {
            get();
        }

        switch (peek())
        {

        case '\0':
            m_tag = Tag::eof;
            break;

        case '0' ... '9':
            do
            {
                m_text += get();
            } while (peek() >= '0' && peek() <= '9');
            if (peek() == '.')
            {
                do
                {
                    m_text += get();
                } while (peek() >= '0' && peek() <= '9');
                m_tag = Tag::real;
                if (peek() == 'e')
                {
                    m_text += get();
                    if (peek() == '-' || peek() == '+')
                    {
                        m_text += get();
                    }
                    if (!(peek() >= '0' && peek() <= '9'))
                    {
                        m_text = "expected a numeral after exponent";
                        m_tag  = Tag::invalid_token;
                    }
                    else
                    {
                        do
                        {
                            m_text += get();
                        } while (peek() >= '0' && peek() <= '9');
                    }
                }
                if (m_tag == Tag::real)
                {
                    m_real = stod(m_text);
                }
            }
            else
            {
                m_tag     = Tag::integer;
                m_integer = stoll(m_text);
            }
            break;

        case '"':
            get();
            while (peek() != '"')
            {
                m_text += getc();
            }
            get();
            m_tag = Tag::text;
            string_replace(m_text, "\\\\", "\\");
            string_replace(m_text, "\\n", "\n");
            string_replace(m_text, "\\t", "\t");
            break;

        case '{':
            get();
            if (peek() == '*')
            {
                get();
                while (true)
                {
                    if (get() == '*' && peek() == '}')
                    {
                        get();
                        goto again;
                    }
                }
            }
            else
            {
                m_text = "expected: *";
                m_tag  = Tag::invalid_token;
            }
            break;

        case '+':
            m_text += get();
            m_tag = Tag::k_plus;
            break;
        case '-':
            m_text += get();
            m_tag = Tag::k_minus;
            break;
        case '*':
            m_text += get();
            m_tag = Tag::k_mul;
            break;
        case '/':
            m_text += get();
            m_tag = Tag::k_div;
            break;
        case '%':
            m_text += get();
            m_tag = Tag::k_mod;
            break;
        case '=':
            m_text += get();
            m_tag = Tag::k_eq;
            break;
        case '<':
            m_text += get();
            switch (peek())
            {
            case '>':
                m_text += get();
                m_tag = Tag::k_neq;
                break;
            case '=':
                m_text += get();
                m_tag = Tag::k_lte;
                break;
            default:
                m_tag = Tag::k_lt;
            }
            break;
        case '>':
            m_text += get();
            if (peek() == '=')
            {
                m_text += get();
                m_tag = Tag::k_gte;
            }
            else
            {
                m_tag = Tag::k_gt;
            }
            break;
        case '(':
            m_text += get();
            m_tag = Tag::k_l_paren;
            break;
        case ')':
            m_text += get();
            m_tag = Tag::k_r_paren;
            break;
        case '[':
            m_text += get();
            m_tag = Tag::k_l_sqbr;
            break;
        case ']':
            m_text += get();
            m_tag = Tag::k_r_sqbr;
            break;
        case '.':
            m_text += get();
            m_tag = Tag::k_dot;
            break;
        case ',':
            m_text += get();
            m_tag = Tag::k_comma;
            break;
        case ';':
            m_text += get();
            m_tag = Tag::k_semicolon;
            break;
        case ':':
            m_text += get();
            if (peek() == '=')
            {
                m_text += get();
                m_tag = Tag::k_assign;
            }
            else
            {
                m_tag = Tag::k_colon;
            }
            break;

        case 'a' ... 'z':
            do
            {
                m_text += get();
            } while ((peek() >= 'a' && peek() <= 'z') ||
                     (peek() >= '0' && peek() <= '9') || peek() == '_');
            if (m_text == "or")
            {
                m_tag = Tag::k_or;
            }
            else if (m_text == "and")
            {
                m_tag = Tag::k_and;
            }
            else if (m_text == "not")
            {
                m_tag = Tag::k_not;
            }
            else if (m_text == "if")
            {
                m_tag = Tag::k_if;
            }
            else if (m_text == "then")
            {
                m_tag = Tag::k_then;
            }
            else if (m_text == "else")
            {
                m_tag = Tag::k_else;
            }
            else if (m_text == "of")
            {
                m_tag = Tag::k_of;
            }
            else if (m_text == "while")
            {
                m_tag = Tag::k_while;
            }
            else if (m_text == "do")
            {
                m_tag = Tag::k_do;
            }
            else if (m_text == "begin")
            {
                m_tag = Tag::k_begin;
            }
            else if (m_text == "end")
            {
                m_tag = Tag::k_end;
            }
            else if (m_text == "var")
            {
                m_tag = Tag::k_var;
            }
            else if (m_text == "array")
            {
                m_tag = Tag::k_array;
            }
            else if (m_text == "procedure")
            {
                m_tag = Tag::k_procedure;
            }
            else if (m_text == "function")
            {
                m_tag = Tag::k_function;
            }
            else if (m_text == "program")
            {
                m_tag = Tag::k_program;
            }
            else if (m_text == "return")
            {
                m_tag = Tag::k_return;
            }
            else
            {
                m_tag = Tag::id;
            }
            break;

        default:
            while (!whitespace())
            {
                m_text += getc();
            }

            m_text = fmt::format("invalid token: {}", m_text);
            m_tag  = Tag::invalid_token;
        }
    }
    catch (const std::istream::failure& e)
    {
        m_text = e.what();
        m_tag  = Tag::fatal_error;
    }

    return m_tag;
}

void Scanner::reset()
{
    m_in->clear();
    m_in->seekg(m_start);

    m_line = 1;
    m_col  = 1;
    m_tag  = Tag::fatal_error;
}

auto Scanner::line() const -> i64
{
    return m_line;
}

auto Scanner::col() const -> i64
{
    return m_col;
}

auto Scanner::tag() const -> Tag
{
    return m_tag;
}

auto Scanner::text() const -> std::string
{
    return m_text;
}

auto Scanner::integer() const -> i64
{
    return m_integer;
}

auto Scanner::real() const -> f64
{
    return m_real;
}

// ----------------------------------------------------------------------------

auto Scanner::getc() -> char
{
    char ret = static_cast<char>(m_in->get());
    if (ret == '\n')
    {
        m_line++;
        m_col = 1;
    }
    else
    {
        m_col++;
    }

    return ret;
}

auto Scanner::get() -> char
{
    return char(tolower(getc()));
}

auto Scanner::peekc() -> char
{
    char ret = 0;
    try
    {
        ret = static_cast<char>(m_in->peek());
    }
    catch (const std::istream::failure& e)
    {
        ret = '\0';
    }
    return ret;
}

auto Scanner::peek() -> char
{
    return static_cast<char>(tolower(peekc()));
}

auto Scanner::whitespace() -> b8
{
    return peek() == ' ' || peek() == '\t' || peek() == '\n';
}

Scanner::operator std::string() const
{
    std::string ret = fmt::format("{}:{}:{}", m_tag, m_line, m_col);
    switch (m_tag)
    {
    case Tag::invalid_token:
    case Tag::fatal_error:
    case Tag::id:
        return fmt::format("{}: {}", ret, m_text);
    case Tag::text:
        return fmt::format("{}: \"{}\"", ret, m_text);
    case Tag::integer:
        return fmt::format("{}: {}", ret, m_integer);
    case Tag::real:
        return fmt::format("{}: {}", ret, m_real);
    default:
        return fmt::format("{}", ret);
    }
}
