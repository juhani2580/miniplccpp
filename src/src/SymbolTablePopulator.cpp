#include "SymbolTablePopulator.hpp"

SymbolTablePopulator::SymbolTablePopulator(SymbolTable& symbol_table) :
    AstVisitor(symbol_table)
{
}

void SymbolTablePopulator::visit(SimpleTypeAst& /*ast*/)
{
}

void SymbolTablePopulator::visit(ArrayTypeAst& /*ast*/)
{
}

void SymbolTablePopulator::visit(IntegerExprAst& /*ast*/)
{
}

void SymbolTablePopulator::visit(RealExprAst& /*ast*/)
{
}

void SymbolTablePopulator::visit(TextExprAst& /*ast*/)
{
}

void SymbolTablePopulator::visit(IdExprAst& /*ast*/)
{
}

void SymbolTablePopulator::visit(NotExprAst& /*ast*/)
{
}

void SymbolTablePopulator::visit(BinaryExprAst& /*ast*/)
{
}

void SymbolTablePopulator::visit(CallExprAst& /*ast*/)
{
}

void SymbolTablePopulator::visit(ReturnAst& /*ast*/)
{
}

void SymbolTablePopulator::visit(IfAst& /*ast*/)
{
}

void SymbolTablePopulator::visit(WhileAst& /*ast*/)
{
}

void SymbolTablePopulator::visit(CallAst& /*ast*/)
{
}

void SymbolTablePopulator::visit(AssignAst& /*ast*/)
{
}

void SymbolTablePopulator::visit(VarDeclAst& ast)
{
    for (auto&& id : ast.ids)
    {
        insert(id, ast.type->type());
    }
}

void SymbolTablePopulator::visit(BlockAst& ast)
{
    for (auto&& stmt : ast.stmts)
    {
        stmt->accept(*this);
    }
}

void SymbolTablePopulator::visit(ScopedBlockAst& ast)
{
    ast.block->accept(*this);
}

void SymbolTablePopulator::visit(ParameterAst& ast)
{
    Type type = ast.type->type();
    if (ast.is_var)
    {
        type.is_var = true;
    }

    insert(ast.id, type);
}

void SymbolTablePopulator::visit(FunctionAst& ast)
{
    for (auto&& param : ast.params)
    {
        param->accept(*this);
    }
    ast.block->accept(*this);
}

void SymbolTablePopulator::visit(ProgramAst& ast)
{
    // Built-in functions
    insert("read", {Primitive::integer});
    insert("writeln", {Primitive::integer});
    insert("assert", {Primitive::devoid});
    insert("true", {Primitive::integer});
    insert("false", {Primitive::integer});

    // Treat the program name as a type with devoid type,
    // just to make sure functions do not shadow it.
    insert(ast.id, {Primitive::devoid});

    for (auto&& func : ast.funcs)
    {
        Type return_type = func->return_type ? func->return_type->type() :
                                               Type{Primitive::devoid};
        insert(func->id, return_type);
        func->accept(*this);
    }
    ast.block->accept(*this);

    if (m_should_throw)
    {
        throw Error("Populating type table failed.");
    }
}

void SymbolTablePopulator::insert(const std::string& id, Type type)
{
    auto ok = m_symtab.symtab.insert({{scopes.back(), id}, type});
    if (!ok.second)
    {
        fmt::print("ERROR: id `{}` is shadows existing type in scope {}\n",
                   id,
                   scopes.back());
        m_should_throw = true;
        return;
    }

    fmt::print("{}: {}: {}\n", scopes.back(), id, type);
}
