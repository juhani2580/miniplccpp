#pragma once

#include "Alt.hpp"
#include "Ast.hpp"
#include "Scanner.hpp"
#include "Tag.hpp"
#include "Utility.hpp"

struct ParseError : Error
{
    using Error::Error;
};

struct Parser
{
    Parser(Scanner& scanner);
    auto parse() -> std::unique_ptr<ProgramAst>;

private:
    Scanner&        m_scanner;
    std::stack<i64> m_scopes{};

    [[nodiscard]] auto line() const -> i64;
    [[nodiscard]] auto col() const -> i64;
    [[nodiscard]] auto tag() const -> Tag;
    [[nodiscard]] auto integer() const -> i64;
    [[nodiscard]] auto real() const -> f64;
    [[nodiscard]] auto text() const -> std::string;
    [[nodiscard]] auto precedence() const -> i64;

    auto unsafe_scan() -> Tag;
    auto scan() -> Tag;
    auto attempt(Tag token) -> b8;

    auto parse_type() -> std::unique_ptr<TypeAst>;
    auto parse_simple_type() -> std::unique_ptr<SimpleTypeAst>;
    auto parse_array_type() -> std::unique_ptr<ArrayTypeAst>;

    auto parse_expr() -> std::unique_ptr<ExprAst>;
    auto parse_primary_expr() -> std::unique_ptr<ExprAst>;
    auto parse_bin_op_rhs_expr(i64                      expr_precedence,
                               std::unique_ptr<ExprAst> lhs)
        -> std::unique_ptr<ExprAst>;
    auto parse_paren_expr() -> std::unique_ptr<ExprAst>;
    auto parse_unary_expr() -> std::unique_ptr<NotExprAst>;
    auto parse_call_or_id_expr() -> std::unique_ptr<ExprAst>;
    auto parse_call_expr_tail(std::string id) -> std::unique_ptr<CallExprAst>;
    auto parse_integer_expr() -> std::unique_ptr<IntegerExprAst>;
    auto parse_real_expr() -> std::unique_ptr<RealExprAst>;
    auto parse_text_expr() -> std::unique_ptr<TextExprAst>;

    auto parse_stmt() -> std::unique_ptr<StmtAst>;
    auto parse_call_or_assign() -> std::unique_ptr<StmtAst>;
    auto parse_var_decl() -> std::unique_ptr<VarDeclAst>;
    auto parse_return() -> std::unique_ptr<ReturnAst>;
    auto parse_while() -> std::unique_ptr<WhileAst>;
    auto parse_if() -> std::unique_ptr<IfAst>;
    auto parse_scoped_block() -> std::unique_ptr<ScopedBlockAst>;

    auto parse_block() -> std::unique_ptr<BlockAst>;
    auto parse_function() -> std::unique_ptr<FunctionAst>;
    auto parse_program() -> std::unique_ptr<ProgramAst>;
};
