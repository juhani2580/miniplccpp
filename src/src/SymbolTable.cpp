#include "SymbolTable.hpp"

auto SymbolTable::find(const std::vector<i64>& scopes, const std::string& id)
    -> Type
{
    for (auto scope = scopes.rbegin(); scope != scopes.rend(); scope++)
    {
        auto search = symtab.find({*scope, id});
        if (search != symtab.end())
        {
            return search->second;
        }
    }

    return {};
}
