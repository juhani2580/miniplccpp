#pragma once

#include "Alt.hpp"
#include "Tag.hpp"

struct Scanner
{
    Scanner(std::unique_ptr<std::istream> in);
    auto               scan() -> Tag;
    void               reset();
    [[nodiscard]] auto line() const -> i64;
    [[nodiscard]] auto col() const -> i64;
    [[nodiscard]] auto tag() const -> Tag;
    [[nodiscard]] auto text() const -> std::string;
    [[nodiscard]] auto integer() const -> i64;
    [[nodiscard]] auto real() const -> f64;
    explicit           operator std::string() const;

private:
    std::unique_ptr<std::istream> m_in{};
    std::istream::pos_type        m_start{};
    i64                           m_line = 1;
    i64                           m_col  = 1;
    Tag                           m_tag  = Tag::fatal_error;
    i64                           m_integer{};
    f64                           m_real{};
    std::string                   m_text{};

    auto getc() -> char;
    auto get() -> char;
    auto peekc() -> char;
    auto peek() -> char;
    auto whitespace() -> b8;
};

template <>
struct fmt::formatter<Scanner> : fmt::formatter<std::string>
{
    template <typename FormatContext>
    auto format(const Scanner& sc, FormatContext& ctx)
    {
        return formatter<std::string>::format(static_cast<std::string>(sc),
                                              ctx);
    }
};
