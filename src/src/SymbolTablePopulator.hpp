#pragma once

#include "Alt.hpp"
#include "Ast.hpp"
#include "SymbolTable.hpp"
#include "Type.hpp"

struct SymbolTablePopulator final : AstVisitor
{
    SymbolTablePopulator(SymbolTable& symbol_table);

    void visit(SimpleTypeAst& /*ast*/) override;
    void visit(ArrayTypeAst& /*ast*/) override;
    void visit(IntegerExprAst& /*ast*/) override;
    void visit(RealExprAst& /*ast*/) override;
    void visit(TextExprAst& /*ast*/) override;
    void visit(IdExprAst& /*ast*/) override;
    void visit(NotExprAst& /*ast*/) override;
    void visit(BinaryExprAst& /*ast*/) override;
    void visit(CallExprAst& /*ast*/) override;
    void visit(ReturnAst& /*ast*/) override;
    void visit(IfAst& /*ast*/) override;
    void visit(WhileAst& /*ast*/) override;
    void visit(CallAst& /*ast*/) override;
    void visit(AssignAst& /*ast*/) override;
    void visit(VarDeclAst& ast) override;
    void visit(BlockAst& ast) override;
    void visit(ScopedBlockAst& ast) override;
    void visit(ParameterAst& ast) override;
    void visit(FunctionAst& ast) override;
    void visit(ProgramAst& ast) override;

private:
    b8 m_should_throw = false;

    void insert(const std::string& id, Type type);
};
