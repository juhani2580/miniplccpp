#include "SemanticAnalyzer.hpp"

SemanticAnalyzer::SemanticAnalyzer(SymbolTable& symtab) : AstVisitor(symtab)
{
}

void SemanticAnalyzer::visit(SimpleTypeAst& ast)
{
    m_ret = ast.type();
}

void SemanticAnalyzer::visit(ArrayTypeAst& ast)
{
    m_ret = ast.type();
    ast.size->accept(*this);
    m_ret.array_size = m_ret_integer;
}

void SemanticAnalyzer::visit(IntegerExprAst& ast)
{
    m_ret         = {Primitive::integer};
    m_ret_integer = ast.value;
}

void SemanticAnalyzer::visit(RealExprAst& /*ast*/)
{
    m_ret = {Primitive::real};
}

void SemanticAnalyzer::visit(TextExprAst& /*ast*/)
{
    m_ret = {Primitive::text};
}

void SemanticAnalyzer::visit(IdExprAst& ast)
{
    if (ast.index)
    {
        ast.index->accept(*this);
        if (!m_ret.is_integer())
        {
            semantic_error("IdExprAst: index must be an integer expression\n");
        }
    }

    Type type_kind = find(ast.id);
    if (type_kind.is_invalid())
    {
        semantic_error("IdExprAst: `{}` missing from symbol table\n", ast.id);
    }

    m_ret = type_kind;
}

void SemanticAnalyzer::visit(NotExprAst& ast)
{
    ast.rhs->accept(*this);
    if (!m_ret.is_integer())
    {
        semantic_error("NotExprAst: expected integer expression\n");
    }
}

void SemanticAnalyzer::visit(BinaryExprAst& ast)
{
    ast.lhs->accept(*this);
    Type lhs = m_ret;

    ast.rhs->accept(*this);
    Type rhs = m_ret;

    if (((lhs.is_integer() && rhs.is_integer()) ||
         (lhs.is_real() && rhs.is_real())) &&
        (ast.op == Tag::k_plus || ast.op == Tag::k_minus ||
         ast.op == Tag::k_mul || ast.op == Tag::k_div || ast.op == Tag::k_mod))
    {
        m_ret = {Primitive::integer};
    }
    else if ((lhs.is_text() && rhs.is_text()) && ast.op == Tag::k_plus)
    {
        m_ret = {Primitive::text};
    }
    else if (((lhs.is_integer() && rhs.is_integer()) &&
              (ast.op == Tag::k_or || ast.op == Tag::k_and)) ||

             (lhs == rhs && (ast.op == Tag::k_eq || ast.op == Tag::k_neq ||
                             ast.op == Tag::k_lt || ast.op == Tag::k_lte ||
                             ast.op == Tag::k_gt || ast.op == Tag::k_gte)))
    {
        m_ret = {Primitive::integer};
    }
    else
    {
        semantic_error("BinaryExprAst: bad types");
        m_ret = {};
    }
}

void SemanticAnalyzer::visit(CallExprAst& ast)
{
    // TODO: check the arguments too

    Type type_kind = find(ast.id);
    if (type_kind.is_invalid())
    {
        semantic_error("CallExprAst: `{}` missing from symbol table\n", ast.id);
    }
}

void SemanticAnalyzer::visit(ReturnAst& ast)
{
    // TODO: check for function return value
    static_cast<void>(ast);
}

void SemanticAnalyzer::visit(IfAst& ast)
{
    ast.condition->accept(*this);
    if (!m_ret.is_integer())
    {
        semantic_error("IfAst: expected integer expression\n");
    }

    ast.then_stmt->accept(*this);
    if (ast.else_stmt)
    {
        ast.else_stmt->accept(*this);
    }
}

void SemanticAnalyzer::visit(WhileAst& ast)
{
    ast.condition->accept(*this);
    if (!m_ret.is_integer())
    {
        semantic_error("WhileAst: expected integer expression\n");
    }

    ast.stmt->accept(*this);
}

void SemanticAnalyzer::visit(CallAst& ast)
{
    ast.call_expr->accept(*this);
}

void SemanticAnalyzer::visit(AssignAst& ast)
{
    Type assign_type = find(ast.id);
    if (assign_type.is_invalid())
    {
        semantic_error("AssignAst: `{}` missing from symbol table\n", ast.id);
    }

    ast.value->accept(*this);
    if (assign_type != m_ret)
    {
        semantic_error("AssignAst: type mismatch\n");
    }
}

void SemanticAnalyzer::visit(VarDeclAst& ast)
{
    if (ast.type->type().is_invalid() || ast.type->type().is_devoid())
    {
        semantic_error("VarDeclAst: invalid type\n");
    }
}

void SemanticAnalyzer::visit(BlockAst& ast)
{
    for (auto&& stmt : ast.stmts)
    {
        stmt->accept(*this);
    }
}

void SemanticAnalyzer::visit(ScopedBlockAst& ast)
{
    ast.block->accept(*this);
}

void SemanticAnalyzer::visit(ParameterAst& /*ast*/)
{
    // TODO: add params to the symbol table...
}

void SemanticAnalyzer::visit(FunctionAst& ast)
{
    // TODO: check the params too

    ast.block->accept(*this);
}

void SemanticAnalyzer::visit(ProgramAst& ast)
{
    for (auto&& func : ast.funcs)
    {
        func->accept(*this);
    }

    ast.block->accept(*this);

    if (m_should_throw)
    {
        throw Error("Semantic analyzing failed");
    }
    fmt::print("OK\n");
}

// ----------------------------------------------------------------------------

template <typename S, typename... Args>
void SemanticAnalyzer::semantic_error(const S& format_str, Args&&... args)
{
    fmt::print(format_str, args...);
    m_should_throw = true;
}
