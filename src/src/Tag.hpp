#pragma once

#include "Alt.hpp"
#include "Utility.hpp"

#define tag_list                                                               \
    X(fatal_error)                                                             \
    X(invalid_token)                                                           \
    X(eof)                                                                     \
    X(id)                                                                      \
    X(integer)                                                                 \
    X(real)                                                                    \
    X(text)                                                                    \
    X(k_plus)                                                                  \
    X(k_minus)                                                                 \
    X(k_mul)                                                                   \
    X(k_div)                                                                   \
    X(k_mod)                                                                   \
    X(k_eq)                                                                    \
    X(k_neq)                                                                   \
    X(k_lt)                                                                    \
    X(k_gt)                                                                    \
    X(k_lte)                                                                   \
    X(k_gte)                                                                   \
    X(k_l_paren)                                                               \
    X(k_r_paren)                                                               \
    X(k_l_sqbr)                                                                \
    X(k_r_sqbr)                                                                \
    X(k_assign)                                                                \
    X(k_dot)                                                                   \
    X(k_comma)                                                                 \
    X(k_semicolon)                                                             \
    X(k_colon)                                                                 \
    X(k_or)                                                                    \
    X(k_and)                                                                   \
    X(k_not)                                                                   \
    X(k_if)                                                                    \
    X(k_then)                                                                  \
    X(k_else)                                                                  \
    X(k_of)                                                                    \
    X(k_while)                                                                 \
    X(k_do)                                                                    \
    X(k_begin)                                                                 \
    X(k_end)                                                                   \
    X(k_var)                                                                   \
    X(k_array)                                                                 \
    X(k_procedure)                                                             \
    X(k_function)                                                              \
    X(k_program)                                                               \
    X(k_return)

enum struct Tag : i64
{
#define X(item) item,
    tag_list
#undef X
};

// tags starting from this should be keywords
// inline constexpr Tag K_TAG_START = Tag::k_plus;
// #define K_TAG_START Tag::k_plus

template <>
struct fmt::formatter<Tag> : fmt::formatter<std::string>
{
    template <typename FormatContext>
    auto format(const Tag& tag, FormatContext& ctx)
    {
        switch (tag)
        {
        default:
            assert(false);
#define X(item)                                                                \
    case Tag::item:                                                            \
        return formatter<std::string>::format(#item, ctx);
            tag_list
#undef X
        }
    }
};

#undef tag_list
