#pragma once

#include <utility>

#include "Alt.hpp"
#include "SymbolTable.hpp"
#include "Tag.hpp"
#include "Type.hpp"

struct AstVisitor;

struct Ast
{
    virtual ~Ast()                           = default;
    virtual void accept(AstVisitor& visitor) = 0;
};

struct TypeAst;
struct ExprAst;
struct StmtAst;
struct ScopeAst;
struct IntegerExprAst;
struct RealExprAst;
struct TextExprAst;
struct IdExprAst;
struct NotExprAst;
struct BinaryExprAst;
struct CallExprAst;
struct SimpleTypeAst;
struct ArrayTypeAst;
struct ReturnAst;
struct IfAst;
struct WhileAst;
struct CallAst;
struct AssignAst;
struct VarDeclAst;
struct BlockAst;
struct ScopedBlockAst;
struct ParameterAst;
struct FunctionAst;
struct ProgramAst;

struct AstVisitor
{
    AstVisitor(SymbolTable& symtab) : m_symtab(symtab)
    {
    }

    std::vector<i64> scopes{};

    virtual void visit(SimpleTypeAst& ast)  = 0;
    virtual void visit(ArrayTypeAst& ast)   = 0;
    virtual void visit(IntegerExprAst& ast) = 0;
    virtual void visit(RealExprAst& ast)    = 0;
    virtual void visit(TextExprAst& ast)    = 0;
    virtual void visit(IdExprAst& ast)      = 0;
    virtual void visit(NotExprAst& ast)     = 0;
    virtual void visit(BinaryExprAst& ast)  = 0;
    virtual void visit(CallExprAst& ast)    = 0;
    virtual void visit(ReturnAst& ast)      = 0;
    virtual void visit(IfAst& ast)          = 0;
    virtual void visit(WhileAst& ast)       = 0;
    virtual void visit(CallAst& ast)        = 0;
    virtual void visit(AssignAst& ast)      = 0;
    virtual void visit(VarDeclAst& ast)     = 0;
    virtual void visit(BlockAst& ast)       = 0;
    virtual void visit(ScopedBlockAst& ast) = 0;
    virtual void visit(ParameterAst& ast)   = 0;
    virtual void visit(FunctionAst& ast)    = 0;
    virtual void visit(ProgramAst& ast)     = 0;

protected:
    SymbolTable& m_symtab;

    auto find(const std::string& id) const -> Type
    {
        return m_symtab.find(scopes, id);
    }
};

struct TypeAst : virtual Ast
{
    virtual auto type() -> Type = 0;
};

struct ExprAst : virtual Ast
{
};

struct StmtAst : virtual Ast
{
};

struct ScopeAst : virtual Ast
{
    i64 scope = -1;
};

struct IntegerExprAst final : ExprAst
{
    i64 value{};

    void accept(AstVisitor& visitor) override
    {
        visitor.visit(*this);
    }
};

struct RealExprAst final : ExprAst
{
    f64 value{};

    void accept(AstVisitor& visitor) override
    {
        visitor.visit(*this);
    }
};

struct TextExprAst final : ExprAst
{
    std::string value{};

    void accept(AstVisitor& visitor) override
    {
        visitor.visit(*this);
    }
};

struct IdExprAst final : ExprAst
{
    std::string              id{};
    std::unique_ptr<ExprAst> index{};

    void accept(AstVisitor& visitor) override
    {
        visitor.visit(*this);
    }
};

struct NotExprAst final : ExprAst
{
    std::unique_ptr<ExprAst> rhs{};

    void accept(AstVisitor& visitor) override
    {
        visitor.visit(*this);
    }
};

struct BinaryExprAst final : ExprAst
{
    Tag                      op{};
    std::unique_ptr<ExprAst> lhs{};
    std::unique_ptr<ExprAst> rhs{};

    void accept(AstVisitor& visitor) override
    {
        visitor.visit(*this);
    }
};

struct CallExprAst final : ExprAst
{
    std::string                           id{};
    std::vector<std::unique_ptr<ExprAst>> args{};

    void accept(AstVisitor& visitor) override
    {
        visitor.visit(*this);
    }
};

struct SimpleTypeAst final : TypeAst
{
    std::string id{};

    auto primitive() const -> Primitive
    {
        if (id == "integer")
        {
            return Primitive::integer;
        }
        if (id == "real")
        {
            return Primitive::real;
        }
        if (id == "string")
        {
            return Primitive::text;
        }
        return Primitive::devoid;
    }

    auto type() -> Type override
    {
        return {.primitive = primitive()};
    }

    void accept(AstVisitor& visitor) override
    {
        visitor.visit(*this);
    }
};

struct ArrayTypeAst final : TypeAst
{
    std::unique_ptr<IntegerExprAst> size{};
    std::unique_ptr<SimpleTypeAst>  element_type{};

    auto type() -> Type override
    {
        return {
            .primitive  = element_type->primitive(),
            .array_size = size->value,
        };
    };

    void accept(AstVisitor& visitor) override
    {
        visitor.visit(*this);
    }
};

struct ReturnAst final : StmtAst
{
    std::unique_ptr<ExprAst> expr{};

    void accept(AstVisitor& visitor) override
    {
        visitor.visit(*this);
    }
};

struct IfAst final : StmtAst
{
    std::unique_ptr<ExprAst> condition{};
    std::unique_ptr<StmtAst> then_stmt{};
    std::unique_ptr<StmtAst> else_stmt{};

    void accept(AstVisitor& visitor) override
    {
        visitor.visit(*this);
    }
};

struct WhileAst final : StmtAst
{
    std::unique_ptr<ExprAst> condition{};
    std::unique_ptr<StmtAst> stmt{};

    void accept(AstVisitor& visitor) override
    {
        visitor.visit(*this);
    }
};

struct CallAst final : StmtAst
{
    std::unique_ptr<CallExprAst> call_expr{};

    void accept(AstVisitor& visitor) override
    {
        visitor.visit(*this);
    }
};

struct AssignAst final : StmtAst
{
    std::string              id{};
    std::unique_ptr<ExprAst> value{};

    void accept(AstVisitor& visitor) override
    {
        visitor.visit(*this);
    }
};

struct VarDeclAst final : StmtAst
{
    std::vector<std::string> ids{};
    std::unique_ptr<TypeAst> type{};

    void accept(AstVisitor& visitor) override
    {
        visitor.visit(*this);
    }
};

struct BlockAst final : Ast
{
    std::vector<std::unique_ptr<StmtAst>> stmts{};

    void accept(AstVisitor& visitor) override
    {
        visitor.visit(*this);
    }
};

struct ScopedBlockAst final : ScopeAst, StmtAst
{
    std::unique_ptr<BlockAst> block{};

    void accept(AstVisitor& visitor) override
    {
        visitor.scopes.push_back(scope);
        visitor.visit(*this);
        visitor.scopes.pop_back();
    }
};

struct ParameterAst final : Ast
{
    b8                       is_var{};
    std::string              id{};
    std::unique_ptr<TypeAst> type{};

    void accept(AstVisitor& visitor) override
    {
        visitor.visit(*this);
    }
};

struct FunctionAst final : ScopeAst
{
    std::string                                id{};
    std::vector<std::unique_ptr<ParameterAst>> params{};
    std::unique_ptr<TypeAst>                   return_type{};
    std::unique_ptr<BlockAst>                  block{};

    void accept(AstVisitor& visitor) override
    {
        visitor.scopes.push_back(scope);
        visitor.visit(*this);
        visitor.scopes.pop_back();
    }
};

struct ProgramAst final : ScopeAst
{
    std::string                               id{};
    std::vector<std::unique_ptr<FunctionAst>> funcs{};
    std::unique_ptr<ScopedBlockAst>           block{};

    void accept(AstVisitor& visitor) override
    {
        visitor.scopes.push_back(scope);
        visitor.visit(*this);
        visitor.scopes.pop_back();
    }
};
