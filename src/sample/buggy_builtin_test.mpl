program buggy_builtin_test;

begin
    var i : integer;
    {* this should pass a pointer for `i` but does not *}
    read(i);
    writeln(i);
    assert(1);
end.
