program buggy_call_test;

procedure var_fun (var a : integer);
begin
    a := 9;
end;

begin
    var ret : integer;
    {* this should pass a pointer for `ret` but does not *}
    var_fun(ret);
    writeln(ret);
end.
