program call_test;

function fun (a : integer, b : integer) : integer;
begin
    if a = 0 then
        return b;
    else
        return a;
end;

procedure var_fun ();
begin
    writeln(9);
    return;
end;

begin
    var ret : integer;
    ret := fun(0, 1);
    var_fun();
end.
