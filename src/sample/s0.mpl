{*
This is a test program for the compiler
This program should compile nicely!
*}

{* Program name is treated as a typeless symbol in the symbol table *}
program s0;

{* This is a comment! *}
{* Compiler should ignore comments! *}

function fun (a : integer, b : integer) : integer;
begin
    if a = 0 then
        return b;
    else
        return a;
end;

{*
Procedures are compiled into C void functions.
Parameters with "var" are compiled into pointers.
*}
procedure var_fun (a : integer, var b : integer);
begin
    if a = 0 then b := a;
    {* empty return should work too *}
    return;
end;

begin
    var i, foobar : integer;
    var rr : real;
    var ss : string;
    i := 0;
    i := not i;
    rr := 1.23;
    ss := "hello"
    i := 5 + 4 * 8 / 2 - 1;
    if i > 5 then foobar := 99;
    else foobar := 10;
    writeln(foobar);
    while i <= 19 do i := i - 1;
    writeln(foobar);
end.
